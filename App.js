import React, {useState} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {Provider} from 'react-redux';
import {
  NavigationContainer,
  DarkTheme,
  DefaultTheme,
} from '@react-navigation/native';

import AppNavigator from './myApp/navigations/AppNavigator';

import store from './myApp/StoreData/store';
import Context from './myApp/components/Context';
import Colors from './myApp/config/Colors';

const customDarkTheme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    background: Colors.dark.background,
    card: Colors.dark.card,
    text: Colors.dark.text,
    border: Colors.dark.border,
  },
};

const customDefaultTheme = {
  ...DefaultTheme,
  colors: {
    ...DarkTheme.colors,
    background: Colors.light.background,
    card: Colors.light.card,
    text: Colors.light.text,
    border: Colors.light.border,
  },
};

function App(props) {
  const [isEnabled, setIsEnabled] = useState(false);

  const theme = isEnabled ? customDarkTheme : customDefaultTheme;

  return (
    <Context.Provider value={{isEnabled, setIsEnabled}}>
      <Provider store={store}>
        <NavigationContainer theme={theme}>
          <AppNavigator />
        </NavigationContainer>
      </Provider>
    </Context.Provider>
  );
}

export default App;
