import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';

import Colors from '../config/Colors';

function ItemSeparator(props) {
  const {dark} = useTheme();

  const textColor = dark
    ? Colors.postSeparator.dark
    : Colors.postSeparator.light;
  return <View style={[styles.container, {backgroundColor: textColor}]}></View>;
}

const styles = StyleSheet.create({
  container: {
    height: 5,
    width: '100%',
  },
});

export default ItemSeparator;
