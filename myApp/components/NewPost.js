import React from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';

function NewPost({textColor, onPress}) {
  return (
    <View style={styles.createPostContainer}>
      <Image
        style={{height: 35, width: 35, borderRadius: 50}}
        source={require('../assets/neymar.jpg')}
      />
      <TouchableOpacity
        style={[styles.createPost, {borderColor: textColor}]}
        onPress={onPress}>
        <Text style={{color: textColor}}>Type something</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  createPostContainer: {
    flexDirection: 'row',
    margin: 15,
  },

  createPost: {
    borderRadius: 20,
    borderColor: '#000',
    borderWidth: 1,
    marginLeft: 15,
    padding: 7,
    width: '85%',
  },
});

export default NewPost;
