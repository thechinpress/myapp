import React from 'react';
import {Image, TouchableOpacity, StyleSheet} from 'react-native';

function UserList({image}) {
  return (
    <TouchableOpacity style={styles.container}>
      <Image style={styles.image} source={{uri: image}} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    borderColor: 'lightgray',
    borderRadius: 50,
    borderWidth: 2,
    height: 60,
    margin: 10,
    overflow: 'hidden',
    width: 60,
  },

  image: {
    height: '100%',
    width: '100%',
  },
});

export default UserList;
