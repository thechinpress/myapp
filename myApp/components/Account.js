import React from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {useTheme} from '@react-navigation/native';

function Account({image, user}) {
  const {dark} = useTheme();

  const textColor = dark ? '#fff' : '#000';
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{uri: image}} />
      <View style={styles.detail}>
        <TouchableOpacity>
          <Text style={[styles.name, {color: textColor}]}>{user}</Text>
        </TouchableOpacity>
        <Text style={{color: textColor}}>12 m</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 10,
  },

  image: {
    borderRadius: 50,
    height: 40,
    width: 40,
  },
  detail: {
    justifyContent: 'center',
    marginLeft: 10,
  },

  name: {
    fontWeight: '700',
    fontSize: 17,
  },
});

export default Account;
