import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Colors from '../config/Colors';

export default () => {
  const {dark} = useTheme();
  const color = dark ? Colors.text : '#000';
  return color;
};
