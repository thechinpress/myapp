import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../config/Colors';

function UserSetting({iconName, title, iconBackground = '#d60f8a'}) {
  const {dark} = useTheme();
  const textColor = dark ? Colors.dark.text : Colors.light.text;
  return (
    <TouchableOpacity style={styles.container}>
      <View style={[styles.iconContainer, {backgroundColor: iconBackground}]}>
        <Icon name={iconName} size={20} color="#fff" />
      </View>
      <Text style={{color: textColor}}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },

  iconContainer: {
    alignItems: 'center',
    borderRadius: 50,
    height: 40,
    justifyContent: 'center',
    marginRight: 10,
    width: 40,
  },

  title: {
    color: Colors.text,
  },
});

export default UserSetting;
