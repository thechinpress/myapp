import React from 'react';
import {Image, View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Colors from '../config/Colors';

function Card({image, onPress, title, likeCount, onLike}) {
  const {dark} = useTheme();

  const textColor = dark ? Colors.dark.text : Colors.light.text;
  return (
    <View>
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <View style={styles.titleContainer}>
          <Text numberOfLines={2} style={[styles.title, {color: textColor}]}>
            {title}
          </Text>
        </View>
        <Image style={styles.image} source={{uri: image}} />
      </TouchableOpacity>
      <View style={styles.likeContainer}>
        <TouchableOpacity onPress={onLike}>
          {likeCount ? (
            <Text style={{color: '#ff0066'}}>Liked</Text>
          ) : (
            <Text style={{color: textColor}}>Like</Text>
          )}
        </TouchableOpacity>
        <Text>{likeCount}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 280,
    marginTop: 5,
    overflow: 'hidden',
    width: '100%',
    justifyContent: 'flex-end',
  },

  titleContainer: {
    height: '20%',
    justifyContent: 'flex-end',
  },

  title: {
    color: Colors.text,
    fontSize: 17,
    paddingHorizontal: 10,
    paddingBottom: 10,
  },

  image: {
    height: '80%',
    width: '100%',
  },

  likeContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 15,
  },
});

export default Card;
