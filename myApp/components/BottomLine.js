import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';

import Colors from '../config/Colors';

function BottomLine(props) {
  const {dark} = useTheme();

  const textColor = dark ? '#737373' : 'lightgray';
  return <View style={[styles.container, {backgroundColor: textColor}]}></View>;
}

const styles = StyleSheet.create({
  container: {
    height: 1,
    width: '100%',
  },
});

export default BottomLine;
