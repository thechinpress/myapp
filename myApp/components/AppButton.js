import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import Colors from '../config/Colors';

function AppButton({title, onPress, width = '100%'}) {
  return (
    <TouchableOpacity style={[styles.container, {width}]}>
      <Text style={styles.button}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#e60073',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },

  button: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    color: 'lightgray',
  },
});

export default AppButton;
