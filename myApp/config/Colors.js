export default {
  light: {
    background: '#f2f2f2',
    text: '#1a1a1a',
    card: '#fff',
    border: '#f2f2f2',
  },
  dark: {
    background: '#1a1a1a',
    text: '#f2f2f2',
    card: '#000',
    border: '#1a1a1a',
  },
  postSeparator: {
    dark: '#404040',
    light: '#e6e6e6',
  },
  newText: {
    dark: '#999',
    light: '#666',
  },
};
