import React, {useState, useEffect} from 'react';
import {
  Image,
  ActivityIndicator,
  Text,
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {useTheme} from '@react-navigation/native';

import Colors from '../config/Colors';

function SportScreen({navigation}) {
  const [isLoading, setIsLoading] = useState(false);

  const {dark} = useTheme();

  const color = dark ? Colors.dark.text : Colors.light.text;

  const [leagues, setLeagues] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    fetch(
      'https://apiv3.apifootball.com/?action=get_leagues&APIkey=3ffe142b633fd74e634d2a7194d0f7d123bc930d7df84b4630a317b7e1292d54',
    )
      .then(res => res.json())
      .then(data => {
        setLeagues(data);

        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return (
      <View style={{alignItems: 'center', flex: 1, justifyContent: 'center'}}>
        <ActivityIndicator size="large" color="gray" />
      </View>
    );
  }

  return (
    <ScrollView style={styles.container}>
      {leagues.map(league => (
        <TouchableOpacity
          style={styles.innerContainer}
          key={league.league_id}
          onPress={() => navigation.navigate('SportDetail', {league})}>
          <Image style={styles.image} source={{uri: league.league_logo}} />
          <Text style={{color: color}}>{league.league_name}</Text>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {},
  innerContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    margin: 10,
  },
  image: {
    borderRadius: 50,
    height: 40,
    marginRight: 10,
    width: 40,
  },
});

export default SportScreen;
