// import {NavigationContainer} from '@react-navigation/native';
// import React, {useState, useEffect} from 'react';
// import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';

// function LeagueScreen({route, navigation}) {
//   const {country} = route.params;
//   const [leagues, setLeagues] = useState([]);

//   useEffect(() => {
//     fetch(
//       `https://apiv3.apifootball.com/?action=get_leagues&country_id=${country.country_id}&APIkey=3ffe142b633fd74e634d2a7194d0f7d123bc930d7df84b4630a317b7e1292d54`,
//     )
//       .then(res => res.json())
//       .then(data => {
//         setLeagues(data);
//       });
//   }, []);

//   return (
//     <View style={styles.container}>
//       {leagues.map(league => (
//         <TouchableOpacity
//           key={league.league_id}
//           style={styles.leagueContainer}
//           onPress={() => navigation.navigate('SportDetail', {league})}>
//           <Image style={styles.image} source={{uri: league.league_logo}} />
//           <Text>{league.league_name}</Text>
//         </TouchableOpacity>
//       ))}
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     margin: 20,
//   },

//   leagueContainer: {
//     flexDirection: 'row',
//     marginVertical: 15,
//   },

//   image: {
//     height: 30,
//     marginHorizontal: 10,
//     width: 30,
//   },
// });

// export default LeagueScreen;
