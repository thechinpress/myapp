import React from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';

import AppButton from '../components/AppButton';

function Profile(props) {
  return (
    <>
      <TouchableOpacity>
        <Image
          style={{height: 220, width: '100%'}}
          source={require('../assets/back.jpg')}
        />
      </TouchableOpacity>
      <View style={styles.profileContainer}>
        <View style={styles.followContainer}>
          <Text style={styles.number}>3.4k</Text>
          <Text style={styles.follow}>Follower</Text>
        </View>
        <Image
          style={styles.profile}
          source={require('../assets/neymar.jpg')}
        />
        <View style={styles.followContainer}>
          <Text style={styles.number}>290</Text>
          <Text style={styles.follow}>Following</Text>
        </View>
      </View>

      <View style={styles.profileNameContainer}>
        <Text style={styles.name}>Salai Lian Cung Thawng</Text>
        <Text style={styles.bio}>
          Founder of One In Chin App, android and ios, web developer, fond of
          playing soccer!
        </Text>
      </View>
      <View style={styles.btnContainer}>
        <AppButton title="Follow" width="30%" />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  profileContainer: {
    flexDirection: 'row',
    position: 'absolute',
    top: '26%',
    alignItems: 'center',
    alignSelf: 'center',
  },

  profile: {
    borderRadius: 50,
    height: 80,
    width: 80,
  },

  followContainer: {
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop: 45,
  },

  number: {
    fontWeight: '700',
  },

  profileNameContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 60,
    width: 250,
  },

  name: {
    fontWeight: '700',
  },

  bio: {
    color: 'gray',
    paddingTop: 5,
    textAlign: 'center',
  },

  btnContainer: {
    alignItems: 'center',
    marginTop: 15,
  },
});

export default Profile;
