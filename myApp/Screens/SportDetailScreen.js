import React, {useState, useEffect} from 'react';
import {
  ActivityIndicator,
  Text,
  ScrollView,
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {useTheme} from '@react-navigation/native';

import BottomLine from '../components/BottomLine';
import Colors from '../config/Colors';

const width = Dimensions.get('window').width;

function SportDetailScreen({route}) {
  const {league} = route.params;
  const [standings, setStandings] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const {dark} = useTheme();

  const color = dark ? Colors.dark.text : Colors.light.text;

  useEffect(() => {
    setIsLoading(true);
    fetch(
      `https://apiv3.apifootball.com/?action=get_standings&league_id=${league.league_id}&&APIkey=3ffe142b633fd74e634d2a7194d0f7d123bc930d7df84b4630a317b7e1292d54`,
    )
      .then(res => res.json())
      .then(data => {
        setStandings(data);

        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return (
      <View style={{alignItems: 'center', flex: 1, justifyContent: 'center'}}>
        <ActivityIndicator size="large" color="gray" />
      </View>
    );
  }

  return (
    <ScrollView style={styles.container}>
      <View style={styles.innerContainer} key={new Date()}>
        <Text style={[styles.teamHeader, {color: color}]}>Team</Text>
        <Text style={[styles.tableHeader, {color: color}]}>PI</Text>
        <Text style={[styles.tableHeader, {color: color}]}>W</Text>
        <Text style={[styles.tableHeader, {color: color}]}>D</Text>
        <Text style={[styles.tableHeader, {color: color}]}>L</Text>
        <Text style={[styles.tableHeader, {color: color}]}>Pts</Text>
      </View>
      {standings.map(item => (
        <>
          <TouchableOpacity style={styles.teamContainer} key={item.team_id}>
            <Text style={{width: 20, textAlign: 'center', color: color}}>
              {item.overall_league_position}
            </Text>
            <Image style={styles.image} source={{uri: item.team_badge}} />
            <Text style={[styles.teamName, {color: color}]}>
              {item.team_name}
            </Text>
            <Text style={[styles.info, {color: color}]}>
              {item.overall_league_payed}
            </Text>
            <Text style={[styles.info, {color: color}]}>
              {item.home_league_W}
            </Text>
            <Text style={[styles.info, {color: color}]}>
              {item.home_league_D}
            </Text>
            <Text style={[styles.info, {color: color}]}>
              {item.home_league_L}
            </Text>
            <Text style={[styles.info, {color: color}]}>
              {item.overall_league_PTS}
            </Text>
          </TouchableOpacity>
          <BottomLine />
        </>
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {},

  innerContainer: {
    flexDirection: 'row',
    marginVertical: 10,
  },

  teamHeader: {
    fontWeight: '700',
    textAlign: 'center',
    width: width / 2.5,
  },

  tableHeader: {
    width: width / 8.5,
    textAlign: 'center',
    fontWeight: '700',
  },
  teamContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 35,
    marginLeft: 5,
  },
  image: {
    height: 20,
    marginHorizontal: 5,
    width: 20,
  },

  teamName: {
    width: width / 4.5,
  },
  info: {
    textAlign: 'center',
    width: width / 8,
  },
});

export default SportDetailScreen;
