import React, {useEffect, useState} from 'react';

import {
  Image,
  TextInput,
  Text,
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {launchImageLibrary} from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Colors from '../config/Colors';

function NewPostScreen({navigation}) {
  const [imagePath, setImagePath] = useState();
  const {dark} = useTheme();

  const color = dark ? '#d9d9d9' : '#1a1a1a';
  const addImageColor = dark ? '#262626' : '#e6e6e6';
  const placeholderText = dark ? Colors.newText.light : Colors.newText.dark;
  const background = dark ? '#1a1a1a' : '#d9d9d9';
  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity style={styles.publishContainer}>
          <Text
            style={[
              styles.publish,
              {color: color, backgroundColor: background},
            ]}>
            Publish
          </Text>
        </TouchableOpacity>
      ),
    });
  }, []);

  const handleImage = async () => {
    launchImageLibrary({mediaType: 'photo', quality: 0.6}, response => {
      if (response.didCancel) return;
      setImagePath(response.assets);
    });
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Type something"
          placeholderTextColor={placeholderText}
          multiline
          autoFocus
          color={color}
        />
      </View>
      {imagePath ? (
        <View style={styles.imageContainer}>
          <TouchableOpacity
            style={styles.closeContainer}
            onPress={() => setImagePath(null)}>
            <Icon name="close-box" size={25} color="gray" />
          </TouchableOpacity>
          <Image style={styles.image} source={{uri: imagePath[0].uri}} />
        </View>
      ) : (
        <TouchableOpacity
          onPress={handleImage}
          style={[styles.addImageContainer, {backgroundColor: addImageColor}]}>
          <Text style={{color: color}}>Add Image</Text>
        </TouchableOpacity>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {},

  publishContainer: {
    marginRight: 15,
  },

  publish: {
    borderRadius: 10,
    padding: 10,
  },

  inputContainer: {
    margin: 10,
  },

  addImageContainer: {
    alignItems: 'center',
    height: 200,
    justifyContent: 'center',
    width: '100%',
  },

  imageContainer: {
    height: 300,
    width: '100%',
  },

  image: {
    height: '100%',
    width: '100%',
    resizeMode: 'stretch',
  },

  closeContainer: {
    position: 'absolute',
    zIndex: 1,
    right: 20,
    top: 10,
  },
});

export default NewPostScreen;
