import React, {useContext} from 'react';
import {useTheme} from '@react-navigation/native';
import {
  Text,
  Switch,
  TouchableOpacity,
  Image,
  View,
  StyleSheet,
} from 'react-native';

import UserSetting from '../components/UserSetting';
import Colors from '../config/Colors';
import Context from '../components/Context';

function AccountScreen({navigation}) {
  const {isEnabled, setIsEnabled} = useContext(Context);

  const toggleTheme = () => {
    setIsEnabled(!isEnabled);
  };

  const {dark} = useTheme();

  const textColor = dark ? '#f0f0f0' : '#000';

  return (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('../assets/neymar.jpg')}
          />
        </View>
        <TouchableOpacity
          style={styles.nameContainer}
          onPress={() => navigation.navigate('AccountDetail')}>
          <Text style={[styles.name, {color: textColor}]}>
            Salai Lian Cung Thawng
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.userSettingContainer}>
        <UserSetting iconName="apps" title="Posts" />
        <UserSetting
          iconName="account-group"
          title="Friends"
          iconBackground="#0da4d6"
        />
        <UserSetting iconName="cog" title="Setting" iconBackground="#808080" />
        <UserSetting iconName="logout" title="Log out" iconBackground="#000" />
      </View>
      <View style={styles.themeToggleContainer}>
        <Text style={{color: textColor}}>Preference</Text>
        <View style={styles.themeToggle}>
          <Text style={{color: textColor}}>Theme</Text>
          <Switch
            thumbColor={isEnabled ? '#fff' : '#000'}
            trackColor={{true: '#b3b3b3', false: '#404040'}}
            value={isEnabled}
            onChange={toggleTheme}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  innerContainer: {
    alignItems: 'center',
    marginTop: 50,
  },

  imageContainer: {
    borderRadius: 50,
    height: 100,
    overflow: 'hidden',
    width: 100,
  },

  image: {
    height: '100%',
    width: '100%',
  },

  nameContainer: {
    marginTop: 10,
  },

  name: {
    fontWeight: '700',
  },

  userSettingContainer: {
    marginHorizontal: 20,
    marginTop: 30,
  },

  themeToggleContainer: {
    marginTop: 20,
    marginLeft: 30,
  },

  themeToggle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 20,
  },
});

export default AccountScreen;
