import React, {useEffect, useState} from 'react';
import {FlatList, View, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
import {useTheme} from '@react-navigation/native';

import Account from '../components/Account';
import Card from '../components/Card';
import PostSeparator from '../components/PostSeparator';
import UserList from '../components/UserList';
import BottomLine from '../components/BottomLine';
import NewPost from '../components/NewPost';
import Colors from '../config/Colors';

function HomeScreen({navigation}) {
  const [like, setLike] = useState('');

  const posts = useSelector(state => state.post.post);
  const users = useSelector(state => state.user.user);

  const {dark} = useTheme();

  const textColor = dark ? Colors.newText.light : Colors.newText.dark;

  const handleLike = () => {
    if (like === '' || like === 0) {
      setLike(1);
    }

    if (like >= 1) {
      setLike(value => value - 1);
    }
  };

  return (
    <View style={styles.container}>
      <View>
        <FlatList
          data={users}
          keyExtractor={item => item.id}
          horizontal
          removeClippedSubviews={false}
          renderItem={({item}) => <UserList image={item.image} />}
        />
      </View>
      <BottomLine />
      <NewPost
        textColor={textColor}
        onPress={() => navigation.navigate('NewPost')}
      />
      <BottomLine />
      <FlatList
        data={posts}
        keyExtractor={item => item.id}
        renderItem={({item}) => (
          <>
            <Account user={item.user} image={item.image} />
            <Card
              image={item.image}
              title={item.title}
              onLike={handleLike}
              likeCount={like}
              onPress={() => navigation.navigate('Detail', {item})}
            />
          </>
        )}
        ItemSeparatorComponent={PostSeparator}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default HomeScreen;
