import React from 'react';
import {Image, Text, View, StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Colors from '../config/Colors';

function DetailScreen({route}) {
  const {item} = route.params;
  const {dark} = useTheme();
  const textColor = dark ? Colors.dark.text : Colors.light.text;

  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{uri: item.image}} />
      <Text style={[styles.detail, {color: textColor}]}>Details </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  image: {
    height: 220,
    width: '100%',
  },

  detail: {
    padding: 15,
  },
});

export default DetailScreen;
