import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {useTheme} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import HomeScreen from '../Screens/HomeScreen';
import DetailScreen from '../Screens/DetailScreen';
import NotificationScreen from '../Screens/NotificationScreen';
import AccountScreen from '../Screens/AccountScreen';
import AccountDetailScreen from '../Screens/AccountDetailScreen';
import NewPost from '../Screens/NewPostScreen';
import SportScreen from '../Screens/SportScreen';
import SportDetailScreen from '../Screens/SportDetailScreen';
import LeagueScreen from '../Screens/LeagueScreen';

function AppNavigator(props) {
  const Stack = createStackNavigator();
  const Tab = createBottomTabNavigator();
  const {dark} = useTheme();
  const color = dark ? '#f0f0f0' : '#1a1a1a';

  const HomeNavigator = () => {
    return (
      <Tab.Navigator>
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={{
            tabBarIcon: ({size, color}) => (
              <Icon name="home" size={size} color={color} />
            ),
          }}
        />

        <Tab.Screen
          name="Sports"
          component={SportScreen}
          options={{
            tabBarIcon: ({size, color}) => (
              <Icon name="soccer" size={size} color={color} />
            ),
          }}
        />

        <Tab.Screen
          name="Notification"
          component={NotificationScreen}
          options={{
            tabBarIcon: ({size, color}) => (
              <Icon name="bell" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Account"
          component={AccountScreen}
          options={{
            tabBarIcon: ({size, color}) => (
              <Icon name="account" size={size} color={color} />
            ),
          }}
        />
      </Tab.Navigator>
    );
  };

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Chin"
        component={HomeNavigator}
        options={{
          headerRight: () => (
            <TouchableOpacity style={styles.search}>
              <Text>Search</Text>
            </TouchableOpacity>
          ),
        }}
      />
      <Stack.Screen
        name="AccountDetail"
        component={AccountDetailScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Detail"
        component={DetailScreen}
        options={({route}) => ({title: route.params.item.title})}
      />

      <Stack.Screen
        name="SportDetail"
        component={SportDetailScreen}
        options={({route}) => ({title: route.params.league.league_name})}
      />

      {/* <Stack.Screen
        name="League"
        component={LeagueScreen}
        options={({route}) => ({title: route.params.country.league_name})}
      /> */}

      <Stack.Screen name="NewPost" component={NewPost} />
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  search: {
    marginRight: 25,
  },
});

export default AppNavigator;
