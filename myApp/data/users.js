import Users from '../Classes/Users';

const USERS = [
  new Users(
    '1',
    'https://www.fcbarcelonanoticias.com/uploads/s1/12/55/55/8/neymar-habla-psg.jpeg',
  ),
  new Users(
    '2',
    'https://storage.googleapis.com/afs-prod/media/cab925c7dd054b3ab9350305b2ec1142/3000.jpeg',
  ),
  new Users('3', 'https://i.insider.com/5da711bb695b58359400d9af?width=700'),
  new Users(
    '4',
    'https://c.ndtvimg.com/2020-03/8d36sn1_cristiano-ronaldo-afp_625x300_25_March_20.jpg?q=60',
  ),
  new Users(
    '5',
    'https://static.india.com/wp-content/uploads/2020/06/Cristiano-Ronaldo-becomes-football%E2%80%99s-first-billionaire-beating-Lionel-Messi%C2%A9Twitter.jpg',
  ),
  new Users(
    '6',
    'https://imgresizer.eurosport.com/unsafe/1200x0/filters:format(jpeg):focal(1344x250:1346x248)/origin-imgresizer.eurosport.com/2021/04/29/3123417-64024068-2560-1440.jpg',
  ),
];

export default USERS;
