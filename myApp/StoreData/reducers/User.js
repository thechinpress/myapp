import {createSlice} from '@reduxjs/toolkit';

import USERS from '../../data/users';

const name = 'users';
const initialState = {
  user: USERS,
};

export const userSlice = createSlice({
  name,
  initialState,
});

export default userSlice.reducer;
