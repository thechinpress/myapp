import {createSlice} from '@reduxjs/toolkit';

import POSTS from '../../data/dataItem';
import USERS from '../../data/users';

const name = 'post';
const initialState = {post: POSTS, user: USERS};

export const postSlice = createSlice({
  name,
  initialState,
});

export default postSlice.reducer;
