import {configureStore} from '@reduxjs/toolkit';
import postReducer from './reducers/Post';
import userReducer from './reducers/User';

export default configureStore({
  reducer: {
    post: postReducer,
    user: userReducer,
  },
});
