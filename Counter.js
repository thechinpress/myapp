import React from 'react';
import {Button, Text, View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

import * as counterAction from './store';

const Counter = () => {
  const dispatch = useDispatch();

  const count = useSelector(state => state.counters.counter);
  return (
    <View>
      <Text>{count}</Text>
      <Button
        title="Increment"
        onPress={() => dispatch(counterAction.increment())}
      />
      <Button
        title="Decrement"
        onPress={() => dispatch(counterAction.decrement())}
      />
    </View>
  );
};

export default Counter;
